# Predlog projekta

| | |
|:---|:---|
| **Naziv projekta** | **BoreDoom** |
| **Člani projektne skupine** | Martin Arsovski, Maja Nikoloska, Bogdan Petrović in Aleksandar Cuculoski |
| **Kraj in datum** | Slovenia Ljubljana, 8.3.2019 |



## Povzetek projekta

Glavni cilj pojekta je izdelati spletno stran / aplikacijo, ki bo omogočala uporabnikom konsistentno obaveščenost o novih in različnih dogodkih v njegovi bližini. 
Aplikacija bo poskrbela da dogodki ustrezajo interesom posameznika.
Uporabnik se lahko prijavi na svoj račun, kjer bo podal podatke, ki jih aplikacija bo uporabljala v namenu boljšega delovanja.
BoreDoom je optimizirana in uporabna aplikacija, izdelana s pomočjo sodobnih tehnologij v namenu lažje uporabe.


## 1. Motivacija

 Danes živimo v dinamičnem svetu, polnem stresa in obveznosti. Večkrat pa nimajo idej kako bi najbolj učinkovito izkoristili svoj prosti čas. Včasih nam tudi lahko pomanjka idej kamor bi preživljali čas.
 Kot vsi veste, v današnjem življenju skoraj da ni človeka, ki ne uporablja internet. Zato smo se odločili prilagoditi digitalizaciji in izdelat spletno aplikacijo, ki vam bo prihranila dragoceni čas in vam dala več idej za izpolnevanje svojega dneva. Namesto tega, da bi iskali  vse dogodke v vašem mestu ali v okolici in da ne veste katere boste obiskali, naša aplikacija bo to naredila za vas. Tako boste sigurno dobili najboljše predloge in res uživali v svojem življenju.




## 2. Cilji projekta in pričakovani rezultati

### 2.1 Opis ciljev

Cilj projekta je, da ljudem damo različne zamisli o tem, kako izpolniti svoj prosti čas, če zmanjka idej. Ko začnejo uporabljati našo aplikacijo, podati morajo nekaj osebnih podatkov in značilnosti. S pomočjo algoritma in API-ja, naša aplikacija spremlja lokacijo uporabnika (če ima dovoljenja, seveda) in priporoča lokale v uporabnikovi okolici v skladu z uporabnikovimi interesi, ki jih je pred tem podal in vnesel v iskalnik dogodkov. Končni izdelek je aplikacija, ki bo vedno dostopna in bo skrbela za vaše izpolnjevanje prostega časa. Uporabnik bo rabil zgolj dostop do spleta. Potrudili se bomo, da je naš izdelek optimiziran in kompatibilen za naprave vseh zmogljivosti in da je naša aplikacija fleksibilna in varna. Vsi rezultati iskanja so dogodki, ki se trenutno izvajajo v okolici. Če ste označili da vam je določeni dogodek zanimiv, sprejeli boste nekaj dodatnih informacij glede števila obiskovalcev tega dogodka, ocen ipd. Vse informacije ki jih posredujete se bodo uporabile izključno v vašo korist in nikoli ne bodo zlorabljene na katerikoli način.

### 2.2 Pričakovani rezultati
Pričakovani rezultat je funkcionalna spletna stran/aplikacija. Aplikacija vam najprej sporoča, da vsi uporabniški podatki ostanejo skiti ter da bo vpoštevala zasebnost njenih uporabnikov. S sprejemanjem teh pogojev mora uporabnik izpolniti nekaj vprašanj glede zanimanj, lokacije in mesta prebivalstva. Naslednje številke temeljijo na uporabnikovih interesih in načinu, kako želi  izpolniti svoj  prosti čas. Nato bo imel na voljo izbiro časovnega intervala, ki ga lahko organizira v skladu z našimi predlogami. Na podlagi tega se izvede izbira dogodkov, do katerih lahko dostopa uporabnik. Možna je tudi izbira več različnih aktivnosti. Uporabnik lahko izbere če želi obiskovati različne kulturne in izobraževalne dogodke. Glede na uporabnikovo izbiro dogodkov, aplikacijo se bo prilagodila njegovim interesom in bo ponujala več različnih dejavnosti, za katere predvideva da bodo ustrezale posameznikovi izbiri. Za namen daljnjega izboljšanja naše aplikacije ponuja se tudi včlanitev v skupino, z enakimi ali podobnimi interesi. Ocenjevanjem skupnih interesov in prejšnjih izbir članov skupin, aplikacija algoritmično določi in ponudi urejen seznam dogodkov, ki bo najbolj ustrezal celotni skupini. Pripadnost skupini, seveda ni obvezen del. Vedno obstaja možnost da uporabnik želi svoj prosti čas preživljati sam. Razdalja med uporabnikom in dogodkom je tudi krucijalen del izbire najbolj ustrezajočih dogodkov. Obstaja tudi možnost objavljanja slik in videoposnetkov in če ne verjamete našim ocenam, lahko tudi sami pogledate objave obiskovalcev.
V podatkih, ki jih uporabnik prejme so ime dogodka, lokacija, število potencialnih obiskovalcev (glede na število prodanih kart) in odstotek obiskovanja tega dogodka (glede na obiskovanje vseh dostopnih dogodkov v okolici).

**Primer** : Uporabnik izbere nek časovni interval. Med več možnostmi (kino, nočni klub, razstave, končert Slovenske filharmonije) je označil je da bi rad šel v kino ali v nočni klub. Po nekaj vprašanj glede žanra, aplikacija je zaznala uporabnikovo lokacijo in mu vrnila urejeni seznam dogodkov ki bo najbolj ustrezale skupini. 
Ko eden uporabnik sprejme končni seznam dogodkov, se seznam avtomatično prosledi vsem članom te skupine.




## 3. Projektni načrt


### 3.1 Povzetek razdelitve projekta na aktivnosti

# **Aktivnosti in načrt posameznih aktivnosti**
En dan je enak 0,05PM = 5h preobremenitev, 1PM = 100h.
***
| **Oznaka aktivnosti** | **A1** |
| :-------------------- | :------------------------------- |
**Datum začetka** | **25. 3. 2019**
**Končni datum** | **29. 3. 2019**
**Trajanje (v dnevih)** | **5**
**Naslov aktivnosti** | **Zajem zahtev**
**Obseg aktivnosti v PM** | **0.25PM**
**Cilji** | **Plan za izpolnjevanje zahtev in specifikacija**
**Opis dejavnosti**  | **Na začetku moramo biti realni in objektivni in moramo določiti, kaj lahko pričakujemo na koncu določenega časa izdelave aplikacije. Člani skupine se morajo zavedati za kakšno aplikacijo gre in biti motivirani za skupinski način dela. Dobro je, če člani poznajo medsebojne pomankljivosti in se glede tega bodo razdelili po vlogah.**
**Odvisnosti in omejitve** | **Ni omejitev.**
**Rezultati** | **Zajem zahtev**

***

| **Oznaka aktivnosti** | **A2** |
| :-------------------- | :------------------------------- |
**Datum začetka** | **1. 4. 2019**
**Končni datum** | **2.4.2019**
**Trajanje (v dnevih)** | **2**
**Naslov aktivnosti** | **Porazdelitev vlog**
**Obseg aktivnosti v PM** | **0.1PM**
**Cilji** | **Da ne bo prišlo do konfliktov, vsak študent mora biti ustrezno obaveščen o svojoj odgovornost pri projektu.**
**Opis dejavnosti**  | **Porazdelili bomo vloge vsakega člena skupine glede na veščino in znanje.**
**Odvisnosti in omejitve** | **A2 je odvisna od A1.**
**Rezultati** | **Vsi členi skupine so organizirani in poznajo svoj segment aplikacije na katerem bo delali do konca projekta.**

***

| **Oznaka aktivnosti** | **A3** |
| :-------------------- | :------------------------------- |
**Datum začetka** | **3. 4. 2019**
**Končni datum** | **4. 4. 2019**
**Trajanje (v dnevih)** | **2**
**Naslov aktivnosti** | **Namestitev delovnih okolj**
**Obseg aktivnosti v PM** | **0.1PM**
**Cilji** | **Ustrezna oprema dostopna ekipi.**
**Opis dejavnosti**  | **Vsak član mora poskrbeti, da ima dostop do dovolj zmogljivega računalnika in delovnega prostora. V A3 bomo poskrbeli da so vse potrebne platforme, moduli in tehnologije nameščene na računalnike.**
**Odvisnosti in omejitve** | **A2.**
**Rezultati** | **Člani so pripravljeni za začetek dela in imajo potrebno programsko opremo in prostor.**

***





| **Oznaka aktivnosti** | **A4**  |
| :-------------------- | :------------------------------- |
**Datum začetka** | **5. 4. 2019**
**Končni datum** | **10. 4. 2019**
**Trajanje (v dnevih)** | **4**
**Naslov aktivnosti** | **Nadgradnja obstoječih znanj in veščin**
**Obseg aktivnosti v PM** | **0.2PM**
**Cilji** | **Globoko znanje o tehnologijah in platform potrebnih za uspešno izdelavo projekta.**
**Opis dejavnosti**  | **Vsi člani morajo odkriti svoje pomankljivosti in jih popolniti. Če ne obstajajo značilne pomankljivosti, nadgraditi morajo obstoječe znanje in pomagati drugim članom skupine.**
**Odvisnosti in omejitve** | **A2**
**Rezultati** | **Izboljšano znanje članov.**


***


| **Oznaka aktivnosti** | **A5**  |
| :-------------------- | :------------------------------- |
**Datum začetka** | **1. 4. 2019**
**Končni datum** | **4. 4. 2019**
**Trajanje (v dnevih)** | **4**
**Naslov aktivnosti** | **Načrt arhitekture projekta**
**Obseg aktivnosti v PM** | **0.2PM**
**Cilji** | **Shematski organiziran plan izdelave.**
**Opis dejavnosti**  | **Ko so vsi člani pripravljeni in vsebujejo določeni nivo znanj, moramo izdelati arhitekturni načrt celotnega projekta. Vsi se morajo zavedati tehničnih lastnosti aplikacije ker je na podlagi vsakega uspešnega projekta dobro izdelan načrt.**
**Odvisnosti in omejitve** | **A1**
**Rezultati** | **Arhitekturni načrt aplikacije.**

***




| **Oznaka aktivnosti** | **A6** |
| :-------------------- | :------------------------------- |
**Datum začetka** | **11. 4. 2019**
**Končni datum** | **17. 4. 2019**
**Trajanje (v dnevih)** | **5**
**Naslov aktivnosti** | **Oblikovanje stran aplikacije**
**Obseg aktivnosti v PM** | **0.25PM**
**Cilji** | **Oblikovanje spletne strani**
**Opis dejavnosti**  | **Med oblikovanjem, skupina mora določiti funkcionalnosti in izgled začelnega dela aplikacije. Začetna stran, izgled profila uporabnikov, seznam dogodkov ipd.**
**Odvisnosti in omejitve** | **A4**
**Rezultati** | **Shema strani.**


***

| **Oznaka aktivnosti** | **A7** |
| :-------------------- | :------------------------------- |
**Datum začetka** | **18. 4. 2019**
**Končni datum** | **26. 4. 2019**
**Trajanje (v dnevih)** | **7**
**Naslov aktivnosti** | **Front-end implementacija**
**Obseg aktivnosti v PM** | **0.35PM**
**Cilji** | **Umestitev spletnih strani**
**Opis dejavnosti**  | **Vse kar smo oblikovali in planirali v prejšnji točki bi zdaj morali realizirati in implementirati vse načrtovane funkcionalnosti začelnega sistema našega projekta. Projekt je namišljen kot aplikacija na eni strani (SPA) in uporabili bomo AngularJS.**
**Odvisnosti in omejitve** | **A6**
**Rezultati** | **Funkcionalni začelni sistem.**

***


| **Oznaka aktivnosti** | **A8** |
| :-------------------- | :------------------------------- |
**Datum začetka** | **11. 4. 2019**
**Končni datum** | **17. 4. 2019**
**Trajanje (v dnevih)** | **5**
**Naslov aktivnosti** | **Back-end planiranje**
**Obseg aktivnosti v PM** | **0.25PM**
**Cilji** | **Določanje strukture back-end sistema.**
**Opis dejavnosti**  | **Planiranje izdelave modela in kako bo model integriran v aplikacijo. Skupina mora določiti kako se podatkovna baza vključi v ostatek aplikacije.**
**Odvisnosti in omejitve** | **A4**
**Rezultati** | **Back-end shema**

***


| **Oznaka aktivnosti** | **A9** |
| :-------------------- | :------------------------------- |
**Datum začetka** | **18. 4. 2019**
**Končni datum** | **26. 4. 2019**
**Trajanje (v dnevih)** | **7**
**Naslov aktivnosti** | **Back-end implementacija**
**Obseg aktivnosti v PM** | **0.35PM**
**Cilji** | **Izdelava zalednega sistema.**
**Opis dejavnosti**  | **Izdelava zalednega sistema in povezovanje z ostatkom aplikacije. Kot podatkovno bazo bomo izbrali MongoDB, pa še orodja NodeJS in ExpressJS.**
**Odvisnosti in omejitve** | **A9 je odvisen od A8**
**Rezultati** | **Back-end sistem**

***

| **Oznaka aktivnosti** | **A10** |
| :-------------------- | :------------------------------- |
**Datum začetka** | **29. 4. 2019**
**Končni datum** | **30. 4. 2019**
**Trajanje (v dnevih)** | **2**
**Naslov aktivnosti** | **Integracija in povezovanje zalednih in začelnih sistemov**
**Obseg aktivnosti v PM** | **0.1PM**
**Cilji** | **Back in front end sistemi funkcionirajo skupaj**
**Opis dejavnosti**  | **Ko so zaledni in začelni sistemi uspešno izdelani, moramo ih povezati in intergrirati.**
**Odvisnosti in omejitve** | **A7, A9**
**Rezultati** | **Povezanost front in back end.**


***

| **Oznaka aktivnosti** | **A11** |
| :-------------------- | :------------------------------- |
**Datum začetka** | **1. 5 2019**
**Končni datum** | **3. 5. 2019**
**Trajanje (v dnevih)** | **3**
**Naslov aktivnosti** | **Razvoj priporočilnega algoritma**
**Obseg aktivnosti v PM** | **0.15PM**
**Cilji** | **Funkcionalnosti priporočilnega algoritma.**
**Opis dejavnosti**  | **Planirana je izdelava algoritma, ki bo na določen način priporočal ustrezne lokale in dogodke v uporabnikovi okolici. Algoritem mora razumno in optimalno delovati in bo temeljil na interesovanjih uporabnikov.**
**Odvisnosti in omejitve**      |**A10.**
**Rezultati** | **Priporočilni algoritem**

***




| **Oznaka aktivnosti** | **A12** |
| :-------------------- | :------------------------------- |
**Datum začetka** | **6. 5. 2019**
**Končni datum** | **10. 5. 2019**
**Trajanje (v dnevih)** | **5**
**Naslov aktivnosti** | **Umestitev priporočilnega algoritma**
**Obseg aktivnosti v PM** | **0.25PM**
**Cilji** | **Namestitev priporočilnega algoritma**
**Opis dejavnosti**  | **Algoritem, ki je planiran v prejšnji točki (A11) zdaj bo treba implementirati in povezovati z ostatkom naše aplikacije. Algoritem bo moral ustrezno uporabiti Google Maps API in spremljati lokacijo uporabnika.**
**Odvisnosti in omejitve** | **A11.**
**Rezultati** | **Algoritem za priporočanje mest in lokalov.**

***

| **Oznaka aktivnosti** | **A13** |
| :-------------------- | :------------------------------- |
**Datum začetka** | **13. 5. 2019**
**Končni datum** | **13. 5. 2019**
**Trajanje (v dnevih)** | **1**
**Naslov aktivnosti** | **Integracija Google Maps API.**
**Obseg aktivnosti v PM** | **0,05PM**
**Cilji** | **Ustrezno povezovanje z Google Maps API.**
**Opis dejavnosti**  | **Uvedli bomo Google Maps API, ki bo spremljal uporabnikovo lokacijo. API bo predvsem uporabljal prejšnje implementiran algoritem za priporočanje zanimivih dogodgov in dejanj.**
**Odvisnosti in omejitve** | **A12, A9, A7**
**Rezultati** | **Priporočilni algoritem pravilno uporablja Google Maps API**

***


| **Oznaka aktivnosti** | **A14** |
| :-------------------- | :------------------------------- |
**Datum začetka** | **14. 5. 2019**
**Končni datum** | **15. 5. 2019**
**Trajanje (v dnevih)** | **2**
**Naslov aktivnosti** | **Unit testiranje in popravljanje napak**
**Obseg aktivnosti v PM** | **0.1PM**
**Cilji** | **Prepričanost o pravilnosti programskih rešitev.**
**Opis dejavnosti**  | **Med točko A14 bomo preverjali funkcionalnosti posameznih enot izvorne kode naše aplikacije. Prepričati se moramo, da vsi segmenti delujejo zanesljivo.**
**Odvisnosti in omejitve** | **A13.**
**Rezultati** | **Ispravna izvorna koda.**

***


| **Oznaka aktivnosti** | **A15** |
| :-------------------- | :------------------------------- |
**Datum začetka** | **16. 5. 2019**
**Končni datum** | **17. 5. 2019**
**Trajanje (v dnevih)** | **2**
**Naslov aktivnosti** | **Integracijsko testiranje**
**Obseg aktivnosti v PM** | **0.1PM**
**Cilji** | **Prepričanost o delovanju vseh modulov aplikacije in njihovi medsebojni komunikaciji.**
**Opis dejavnosti**  | **Ko smo uspešno končali A14, lotili se bomo preverjanja posameznih segmentov naše aplikacije in skupne funkcionalnosti. Testirali bomo komunikacijo izmed back-end sistema, front-end sistema, algoritma za priporočanje zanimivih lokacij in API-jev.**
**Odvisnosti in omejitve** | **A14.**
**Rezultati** | **Ispravno delujoči moduli aplikacije.**

***

| **Oznaka aktivnosti** | **A16** |
| :-------------------- | :------------------------------- |
**Datum začetka** | **20. 5. 2019**
**Končni datum** | **21. 5. 2019**
**Trajanje (v dnevih)** | **2**
**Naslov aktivnosti** | **Sistemsko testiranje**
**Obseg aktivnosti v PM** | **0.1PM**
**Cilji** | **Prepričanost o delovanju aplikacije kot celota.**
**Opis dejavnosti**  | **V A16 se moramo prepričati da je naša aplikacija popolnoma delujoča in funkcionalna brez napak. Poskusili bomo različne poizvedbe in vhode. Moramo tudi preveriti ali naša aplikacija ustreza zahtevam in je dovolj zmogljiva.**
**Odvisnosti in omejitve** | **A15.**
**Rezultati** | **Celotna aplikacija deluje.**

***




| **Oznaka aktivnosti** | **A17** |
| :-------------------- | :------------------------------- |
**Datum začetka** | **22. 5. 2019**
**Končni datum** | **23. 5. 2019**
**Trajanje (v dnevih)** | **2**
**Naslov aktivnosti** | **Uporabniško testiranje**
**Obseg aktivnosti v PM** | **0.1PM**
**Cilji** | **Aplikacija prijazna uporabnikom**
**Opis dejavnosti**  | **Najboljšo povratno informacijo bomo dobili od potencialnega uporabnika. Mnenja o prijaznosti, enostavnosti in oblike naše aplikacije so zelo pomembna.**
**Odvisnosti in omejitve** | **A16**
**Rezultati** | **Končni izdelek našega tima.**

***




| **Oznaka aktivnosti** | **A18** |
| :-------------------- | :------------------------------- |
**Datum začetka** | **24. 5. 2019**
**Končni datum** | **24. 5. 2019**
**Trajanje (v dnevih)** | **1**
**Naslov aktivnosti** | **Kontaktiranje in pridobivanje partnerjev**
**Obseg aktivnosti v PM** | **0.05PM**
**Cilji** | **Sledimo prodaji vstopnic. S tem imamo približno informacijo o obiščenosti vsakega dogodka.**
**Opis dejavnosti**  | **Da bi aplikacije učinkovito funkcionirala, začetne podatke morajo posredovati zaposleni v ustanovah kamor se izvajajo dogodki za katere je posameznik zainteresiran. Zaposleni podajo podatke o ceni vstopnic, delovnem času in o povprečnem številu obiskovalcev.**
**Odvisnosti in omejitve** | **A17, ker moramo biti prepričani da je izdelava aplikacije bila uspešna preden začnemo iskati partnerje.**
**Rezultati** | **Pridobitev potrebnih začetnih podatkov.**

***


| **Oznaka aktivnosti** | **A19** |
| :-------------------- | :------------------------------- |
**Datum začetka** | **22. 5. 2019**
**Končni datum** | **23. 5. 2019**
**Trajanje (v dnevih)** | **2**
**Naslov aktivnosti** | **Dokumentacija**
**Obseg aktivnosti v PM** | **0.1PM**
**Cilji** | **Dobro organiziran tekst z informacijami o razvoju**
**Opis dejavnosti**  | **Vsak član mora sporočiti organizirano dokumentacijo svojega segmenta aplikacije. Dobro narejena dokumentacija je pomembna za bodoče verzije našega projekta. Hkrati nam pomaže v situacijah v katerim pride do nepričakovanih problemov.**
**Odvisnosti in omejitve** | **A16.**
**Rezultati** | **Dobro in hronološki organizirano sporočilo razvoja aplikacije.**





















### 3.3 Seznam izdelkov



| Oznaka izdelka | Ime izdelka | Datum izdaje | 
| :--- | :---------------------------- | :--- |
| **A1** | **Začetni načrt aplikacije in zajem zahtev** | **29.3.2019** |
| **A2**       |**Porazdelitev članov po vlogah**       | **2.4.2019**       |
| **A3** | **Nameščena programska oprema in delovno okolje** | **4.4.2019**
| **A4** | **Znanja in veščine za razvoj aplikacije** | **10.4.2019**
|**A5** |  **Načrt arhitekture projekta** | **4.4.2019** 
| **A6** | **Začetna shema strani** | **17.4.2019**
| **A7** | **Front end sistem** | **26.4.2019**
|**A8** | **Shema zalednega sistema** | **17.4.2019**
|**A9** | **Funkcionalni zaledni sistem** | **26.4.2019**
|**A10** | **Medsebojna komunikacija začelnih in zalednih sistemov** | **30.4.2019**
|**A11** | **Plan priporočilnega algoritma** | **3.5.2019**
|**A12** | **Funkcionalni algoritem za priporočanje** | **10.5.2019**
|**A13** | **Pravilno delujoči Google Maps API v integraciji z algoritmom** | **13.5.2019**
|**A14** | **Pravilna izvorna koda** | **15.5.2019**
|**A15** | **Pravilni moduli in njihova medsebojna komunikacija** | **17.5.2019**
|**A16** | **Celotna funkcionalnost aplikacije** | **21.5.2019**
|**A17** | **Uporabniška povratna informacija in končni izdelek** | **23.5.2019**
|**A18** | **Podaci o lokalih iz realne okolice** | **24.5.2019**
|**A19** | **Dokumentacija**| **23.5.2019**


### 3.4 Časovni potek projekta - Ganttov diagram






![Ganttov diagram](../img/ganttgraph.png)



### 3.5 Odvisnosti med aktivnosti - Graf PERT

![Graf PERT](../img/graf-pert.png)



## 4. Obvladovanje tveganj

### 4.1 Identifikacija in analiza tveganj


| Naziv tveganja | Opis tveganja | Tip tveganja | Verjetnost nastopa tveganja | Posledice nastopa tveganja |
| :------------- | :------------ | :----------- | :-------------------------- | :------------------------- |
| **Izguba člana ekipe**      | **Član skupine se je odločil da bo zapustil projekt iz kakršnegakoli razloga.**     | **Ljudje**    | **Nizka** | **Resne** |
| **Neenakost obsega dela članov(neenakomerne zahteve)**            |**Nesorazmernost dela članov. Nekateri člani imajo preveč dela, ostali pa premal. Druga možni problem je ko član, ki je nov ne pozna sistem enako dobro kot ostali člani, kljub temu pa mu je dodeljena enaka količina dela.** |**Ljudje  Zahteve  Organizacija** | **Zmerna** | **Neznatne** |
| **Slaba motivacija clana** |**Član ni dovolj motiviran za delo na projektu. To se lahko zgodi če po njegovem mnenju aplikacija ni dovolj zanimiva ali perspektivna.**  |**Ljudje**|**Zelo nizka**|**Dopustne**|
| **Znanja, veščine in izkušnje članov ne ustrezajo zahtevami projekta** |**Član ne obvlada tehnologijo ali ni navajen na delo v programskem jeziku, ki ga projekt zahteva.** |**Ljudje  Tehnologija  Orodja**|**Zmerna**|**Resne**|
| **Nedovoljna zmogljivost strežnika**  |**Zaradi slabega načrta arhitekture sistema, reševanje določenih problemov ni mogoče.**|**Organizacija  Orodja**|**Visoka**|**Usodne**|
| **Okvara strojne opreme** | **Nezmožnost uporabe računalnikov in strojne opreme.** |**Orodja**|**Nizka**|**Resne**|
| **Slab projektni plan** |**Ni dovolj informacij za izvedbo projekta, specifikacije aplikacije, nejasni cilji, podcenjena velikost, podcenjen čas.**|**Zahteve**|**Zelo visoka**|**Usodne**|
| **Slaba odgovornost partnerjev** |**Podatki, ki jih v aplikacijo vnesejo lastniki prostorov in lokalov so nepopolni.**|**Orodja**|**Zmerna**|**Resne**|
| **Slaba organizacija ekipe**  |**Zgodi se lahko da člani skupine delo ne razdelijo ustrezno, v tem primeru pa lahko več članov dela na istem področju projekta.**|**Zahteve**|**Zelo visoka**|**Resne**|
| **Varnostne grožnje**  |**Vdori in virusi, ki sesujejo aplikacijo in zlorabljajo podatke uporabnikov.**|**Tehnologija**|**Zelo nizka**|**Usodne**|
| **Pogosto spreminjanje specifikacij aplikacije** |**Razvijalci morajo večkrat spreminjati kodo, kar lahko povzroči pomanjkanje časa.**|**Zahteve**|**Zelo visoka**|**Dopustne**|
| **Prekoračitev proračuna**  |**Izdelava produkta rabi več finančnih sredstev, kot je bilo predvideno.**|**Ocenjevanje**|**Zelo visoka**|**Resne**|
| **Preveč časa za posamezne naloge** |**Čas, ki ga skupina rabi za izdelavo posameznega segmenta aplikacije ni dobro ocenjen.**|**Ocenjevanje**|**Visoka**|**Resne**|
| **Konkurencija** |**Na trgu se lahko zgodi da imamo konkurenco. Že obstaja aplikacija ki ima enake podrobnosti kot naša.**|**Tehnologija**|**Zmerna**|**Resne**|

### 4.2 Načrtovanje tveganj



| Tveganje  | Strategija |
| :-------- | :--------- |
| **Izguba člana ekipe**  | **Če pride do izgube člana ekipe, njega lahko nadomestimo z novim članom s podobnimi izkušnjami. Iskali bomo posameznika, ki je ustrezno pripravljen in lahko prevzame vse obveznosti izgubljenega člana. Dokler ga ne najdemo, obveznosti bodo razdeljene med ostalimi člani skupine.**  |
| **Neenakomernost obsega dela članov (nedirenakomerne zahteve)**   | **Ta problem bomo reševali z majhnimi spremembami načrta projektnega plana. Vodja skupine rasporedi delo še enkrat od začetka in poskrbi da vsak član ima dovolj in ne preveč dela.** |
| **Slaba motivacija clana** | **Zamenjamo obveznosti člana ki ni dovolj motiviran. Podamo mu nove obveznosti, ki bi jih boljše in hitrejše reševal.** |
| **Znanja, veščine in izkušnje članov ne ustrezajo zahtevami projekta** | **Član dobi nekaj dni dovolj za boljšo pripravljenost in izpolnjevanje pomanjkljivosti v njegovem dosedanjem znanju. Če posameznik kljub temu še vedno ima težave, pomembno je sodelovanje med vsemi člani skupine in medsebojna izmenjava izkušenj in dopolnjevanje.** |
| **Nedovoljna zmogljivost strežnika** | **Sprememba načrta plana in drugačen pristop izdelave.** |
| **Okvara strojne opreme** | **Strojna oprema se mora zamenjati z novo popraviti obstoječo okvaro.** |
| **Slab projektni plan** | **Vodja mora narediti nekaj sprememb načrta in podati boljšo razlago ciljev. Vsakemu članu predstavi plan vseh obveznosti, ki jih bo moral uspešno dokončati do predvidenega termina.** |
| **Slaba odgovornost partnerjev** | **Pri izbiri partnerja smo zelo previdni in raziskujemo več o njihovih izkušnjah in njihove medsebojne odnose s obstoječimi in preteklimi partnerji.** |
| **Slaba organizacija ekipe** | **Vodja, vsakega člana skupine ekipe posebej natančno seznani z posebnostmi naloge, ki jo mora rešiti, ter med tekom projekta preverja učinkovitost dela posameznika ki naj bi bilo usklajeno s podanimi zadolžitvami.** |
| **Varnostne grožnje** | **Naredimo boljše varnostne sisteme in vse podatke shranjujemo v skrivnosti.** |
| **Pogosto spreminjanje specifikacij aplikacije** | **Vodja na začetku naredi dober načrt z vsemi potrebnimi specifikacijami, ki jih je potrebno spoštovati.** |
| **Prekoračitev proračuna**  | **V načrtu dodamo dodatnih finančnih sredstev. Obstoječo opremo tudi lahko zamenjamo  z cenejšo.** |
| **Preveč časa za posamezne naloge** | **Razširjamo skupino z novimi člani. V primeru da člani skupine potrebujejo več časa, pomembno je spremeniti predviden čas in ga urediti tako, da ustreza posameznikom ter se zavedati da spremenjen čas ne vpliva na ostale dele projekta.** |
| **Konkurencija** | **Preden izdelamo načrt, najprej dobro raziskamo trg in v primeru, da že obstaja podobna aplikacija, dodamo nekaj novih  podrobnosti naši aplikaciji, ki bodo bolj zanimive in privlačne uporabnikom.** |


## 5. Upravljanje projekta


|  Naloga   |   Martin  |   Maja   |  Bogdan  | Aleksandar|
| :-------- | :-------- |:-------- |:-------- | :-------- |
| Povzetek projekta | 30% | 30% | 20% | 20% |
| Motivacija | 100% |  |  |  |
| Cilji in pričakovani rezultati projekta | 80% | 20% |  |  |
| Aktivnosti in načrt posameznih aktivnosti |  |  | 70% | 30% |
| Časovni razpored | 10% |  | 30% | 60% |
| Računanje človek-mesec |  | 30% |  | 70% |
| Seznam izdelkov |  |  | 80% | 20% |
| Ganttov diagram  |  |  | 100% |  |
| Graf PERT |  |  |  | 100% |
| Identifikacija in analiza tveganj | 50% | 50% |  |  |
| Načrtovanje tveganj | 30% | 70% |  |  |
| Finančni načrt |  | 100% |  |  |

## 6. Predstavitev skupine

* Maja je odgovorna za načrtovanje projekta, oblikovanje strani in front end funkcionalnosti. Sodelovala bo z Martinom. Dobro pozna HTML, CSS in Angular.
* Martin bo delal na razvoju front end-a z Majo, in na koncu projekta bo poskrbel za testiranje. Martin je dobro organiziran in motiviran naučiti več in je uporabljal HTML in CSS. Pozna tudi orodja za testiranje (Selenium, Katalon Studio).
* Bogdan je odgovornen za back end pri čemer bo sodeloval z Aleksandrom. Tudi bo delal na namestitvi podatkovne baze in integracijo projekta z Google Maps API-jem. Uporabljal bo ogrodja kod NodeJS, podatkovno bazo MongoDB in ExpressJS.
* Aleksandar se skupaj z Bogdanom bo okvarjal z back end-om. Po poskrbel za izdelavo algoritma za priporočanje lokacij in manjši del testiranja aplikacije. Pozna jezik Java in ima nekaj izkušenj na področju MEAN sklada. 




## 7. Finančni načrt - COCOMO II ocena



|  Tip funkcionalnosti  |  Naziv funkcionalnosti	 |  Obseg  |  Utež  |
| :-------- | :-------- |:-------- |:--------|
| EI | Prijava v system | L | 3 |
| EI | Kreiranje lokcaije | L | 3 |
| EI | Vzdrževanje uporabnikov | A | 4 |
| EI | Vzdrževanje lokacije | A | 4 |
| EI | Posodobljanje lokacije | A | 4 |
| EI | Posodobljanje uporabnikov | L | 3 |
| EO | Prikaz seznama uporabnikov | H | 7 |
| EO | Prikaz seznama lokacije | H | 7 |
| ILF | Podatkovna baza | L | 7 |

Bomo aplikacijo razvijali v jeziku JavaScript: 42 * 47 = 1974





**Finančni načrt**






| **Akcija** | **Naslov aktivnosti** | **Obseg aktivnost v PM** | **Neposredni stroški** | **Posredni stroški** | **Skupaj** |
| :-------- | :-------- |:-------- |:--------|:-------- |:--------|
| A1 | Začetni načrt aplikacije in zajem zahtev | 0,25 | 200 | 40 | 240 |
| A2 | Porazdelitev članov po vlogah | 0,1 | 80 | 16 | 96 |
| A3 | Nameščena programska oprema in delovno okolje | 0,1 | 80 | 16 | 96 |
| A4 | Znanja in veščine za razvoj aplikacije | 0,2 | 160 | 32 | 192 |
| A5 | Načrt arhitekture projekta | 0,2 | 160 | 30 | 190 |
| A6 | Začetna shema strani | 0,25 | 200 | 44 | 244 |
| A7 | Front end sistem | 0,35 | 280 | 54 | 334 |
| A8 | Shema zalednega sistema | 0,25 | 200 | 40 | 240 |
| A9 | Funkcionalni zaledni sistem | 0,35 | 280 | 56 | 336 |
| A10 | Medsebojna komunikacija začelnih in zalednih sistemov | 0,1 | 80 | 16 | 96 |
| A11 | Plan priporočilnega algoritma | 0,15 | 120 | 24 | 144 |
| A12 | Funkcionalni algoritem za priporočanje | 0,25 | 200 | 38 | 238 |
| A13 | Pravilno delujoči Google Maps API v integracije z algoritmom | 0,05 | 40 | 10 | 50 |
| A14 | Pravilna izvorna koda | 0,1 | 80 | 16 | 96 |
| A15 | Pravilni moduli in njihova medsebojna komunikacija | 0,1 | 80 | 16 | 96 |
| A16 | Celotna funkcionalnost aplikacije | 0,1 | 80 | 16 | 96 |
| A17 | Uporabniška povratna informacija in končni izdelek | 0,1 | 80 | 16 | 96 |
| A18 | Podaci o lokalih iz realne okolice | 0,05 | 40 | 8 | 48 |
| A19 | Dokumentacija | 0,1 | 100 | 16 | 116 |






**Specifikacija stroškov**






| **Verzija** | **Naslov aktivnosti** | **Storitve** | **Investicije** | **Potni stroški** |
|:-------- |:--------|:-------- |:--------|:-------- |
| A19.1 | Dokumentacija |  | 20-papir |  |




Pri finančnem načrtu je eno ura dela nastavljeno na 8 eur. (1 PM = 100h, torej 1 PM = 800 eur)

Posredni stroški so nastavljeni na 20% od stroške dela.

Skupaj, imamo 3,1 PM in 3044 EUR skupnih stroškov(posredni in neposredni stroški skupaj). Dodamo še dodatne stroške 42 EUR in povečamo proračun za 12% kot ukrep za obvladovanje tveganj.

Končni proračun je 3457 EUR.




![COCOMO II part 1](../img/cocomoII1.png)
![COCOMO II part 2](../img/cocomoII2.png)

 



## Reference

[1]: doc. dr. Dejan Lavbič, **Interaktivna skripta**, https://teaching.lavbic.net/TPO/
[2]:  **COCOMO II**,http://csse.usc.edu/tools/COCOMOII.php